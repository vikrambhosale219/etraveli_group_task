
import React, { useState, useEffect } from "react";
import axios from "axios";
import "../Components/Task.css"

const  Task=()=> {
  const [movies, setMovies] = useState([]);
  const [selectedMovie, setSelectedMovie] = useState(null);
  const [sortOrder, setSortOrder] = useState("year");
  const [filterText, setFilterText] = useState("");

  useEffect(() => {
    axios.get("http://swapi.dev/api/films/?format=json").then(response => {
      setMovies(response.data.results);
    });
  }, []);

  const filteredMovies = movies.filter(movie =>
    movie.title.toLowerCase().includes(filterText.toLowerCase())
  );


  const sortedMovies = filteredMovies.sort((a, b) => {
    if (sortOrder === "year") {
      
      return a.release_date.localeCompare(b.release_date);

    } else {
      return a.episode_id - b.episode_id;
    }
  });

  const handleMovieClick = movie => {
    setSelectedMovie(movie);
  };

  return (
    <div className="App">
      <div className="movie-list">
        <h1>Star Wars Movies</h1>
        <label>
          Sort by:
          <select value={sortOrder} onChange={e => setSortOrder(e.target.value)}>
            <option value="year">Year</option>
            <option value="episode">Episode</option>
          </select>
        </label>
        <br />
        <label>
          Filter by title:
          <input
            type="text"
            value={filterText}
            onChange={e => setFilterText(e.target.value)}
          />
        </label>
        <ul>
          {sortedMovies.map(movie => (
            <li key={movie.episode_id} onClick={() => handleMovieClick(movie)}>
              {movie.title} ({movie.release_date})
            </li>
          ))}
        </ul>
      </div>
      <div className="selected-movie">
        {selectedMovie ? (
          <div>
            <h2>{selectedMovie.title}</h2>
            <p>Episode {selectedMovie.episode_id}</p>
            <p>Director: {selectedMovie.director}</p>
            <p>Release date: {selectedMovie.release_date}</p>
            <p>Opening crawl: {selectedMovie.opening_crawl}</p>
          </div>
        ) : (
          <p>No Movie Selected</p>
        )}
      </div>
    </div>
  );
  
}

export default Task;

